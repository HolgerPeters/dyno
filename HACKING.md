# Hacking Dyno

To build Dyno, execute

    cd dyno
    autoreconf -vif && ./configure --prefix=/opt/guile && make
    . env

See the TODO file for pending tasks.

## Runing Tests


`make -e check` runs the tests
