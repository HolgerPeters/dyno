# Dyno

Dyno is a test-runner for Guile scheme, inspired by Brian Marick's
[Midje](https://github.com/marick/Midje). The general idea of Midje,
and thus Dyno is, that tests appear similarly to how expressions and
results are represented in a REPL session. The expression under test
is separeted by `=>` from the value that it should evaluate to.  A
simple test case asserting that the numbers "one" and "two" add up to
"two" (which of course they don't) looks as follows:

    (dy/fact "numbers should add up"
      (+ 1 2) => 2)

When evaluated, Dyno will report the test failure with contextual
information about what went wrong.

    ERROR "numbers should add up: (+ 1 2) => 2" at (...)
        Expected: 2
          Actual: 3
       asserting: (+ 1 2) => 2

    ...
