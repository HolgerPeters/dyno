(define-module (dyno)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-9)
  #:export (dy/assert
            dy/fact
            dy/facts
            dy/unfinished
            dy/version
            ))


(define (dy/version)
  "0.0.1")

(define-record-type <test-case>
  (make-test-case mod description thunk)
  test-case?
  (mod         test-case-mod)
  (description test-case-description)
  (thunk       test-case-thunk))


(define (make-color cs)
  (if (null? cs)
    ""
    (string-append
      (string #\esc #\[)
      (string-join cs ";" 'infix)
      "m")))


(define (colorify str cs)
  (string-append
    (make-color cs)
    str
    (make-color (list "0"))))


(define (report class message stack-trace)
  (format (current-error-port)
          "~a ~a ~%"
          class
          message)
  (when (not (null? stack-trace))
    (display-backtrace stack-trace (current-error-port))))


(define *fault* (colorify "FAULT" (list "1" "31")))
(define *unimplemented* (colorify "FAULT" (list "0" "33")))
(define *failure* (colorify "FAILURE" (list "0" "31")))
(define *error* (colorify "ERROR" (list "0" "31")))
(define *pass* (colorify "PASS" (list "1" "32")))


(define (report-unimplemented message trace)
  (report *unimplemented* message trace))

(define (report-fault message trace)
  "The inability of a system or component to perform its required functions
  within specified performance requirements"
  (report *fault* message trace))


(define (report-failure message trace)
  "The inability of a system or component to perform its required functions
  within specified performance requirements"
  (report *fault* message trace))


(define (report-error message trace)
  "A discrepancy between a computed, observed, or measured value or condition
  and the true, specified, or theoretically correct value or condition"
  (report *error* message trace))


(define (report-pass message trace)
  "A discrepancy between a computed, observed, or measured value or condition
  and the true, specified, or theoretically correct value or condition"
  (report *pass* message trace))


(define-syntax dy/assert
  (syntax-rules (=)
    ((_ (= a b) message)
     (let* ((a-prime a)
            (b-prime b)
            (loc (current-source-location))
            (lno (cdr (assv 'line loc)))
            (col (cdr (assv 'column loc)))
            (file (cdr (assv 'filename loc)))
            (msg (format #f "~a: ~a => ~a" message (quote a) (quote b))))
       (cond
         ((not (equal? a-prime b-prime))
          (throw 'zassert
                 (format #f "\"~a\" at (~a:~a:~a)~%    Expected: ~a~%      Actual: ~a~%   asserting: ~a => ~a"
                         msg
                         file
                         lno
                         col
                         b-prime
                         a-prime
                         (quote a)
                         (quote b))))
         (else (report-pass msg '())))))
     ((_ expression message)
      (if (expression)
        (report-pass message '())
        (throw 'zassert message)))))


(define (user-desires-checking?)
  "this function may be overwritten iff the necessity arises that tests aren't
  immediately executed upon evaluating the test module"
  #t)


(define *list-of-test-cases* '())


(define-syntax inplace-append!
  (syntax-rules ()
     ((_ d v)
      (set! d (append! d v)))))



(define-syntax single-fact
  (syntax-rules (=>)
    ((_ description got => expected-result)
     (let* ((captured-stack #f)
            (fn-name (lambda ()
                       (catch #t
                         (lambda ()
                           (dy/assert (= got expected-result)
                                      description))
                         (lambda (key . parameters)
                           (case key
                             ((zunfinished)
                              (report-unimplemented parameters captured-stack))
                             ((zassert)
                              (report-error (car parameters) captured-stack))
                             (else
                               (report-failure parameters captured-stack))))
                         (lambda (key . args)
                           (set! captured-stack (make-stack #t))))))
            (cur-mod (current-module))
            (info (make-test-case cur-mod description fn-name)))
       (inplace-append! *list-of-test-cases* (list info))
       (when (user-desires-checking?)
         (fn-name))))))


(define-syntax dy/facts
  (syntax-rules (=>)
    ((_ args ...)
     (dy/fact args ...))))


(define-syntax dy/fact
  (syntax-rules (=> dy/fact)
    ((_ description got => expected)
     (single-fact description got => expected))
    ((_ description (dy/fact sub-description facts ...))
     (dy/fact (string-append description sub-description) facts ...))
    ((_ description (dy/fact sub-description facts ...) more ...)
     (begin
       (dy/fact (string-append description sub-description) facts ...)
       (dy/fact description more ...)))
    ((_ description got => expected more ...)
     (begin
       (single-fact description got => expected)
       (dy/fact description more ...)))))


(define-syntax dy/unfinished
  (syntax-rules ()
     ((_ a)
      (define (a . args)
        (let ((msg (format #f "Procedure ~a is not implemented, but called with arguments ~a" 'a args)))
          (throw 'zunfinished msg))))
     ((_ a b ...)
      (begin
        (dy/unfinished a)
        (dy/unfinished b ...)))))
