\input texinfo   @c -*-texinfo-*-
@c %**start of header
@setfilename dyno.info
@settitle Dyno
@c %**end of header

@set VERSION 0.0.1
@set UPDATED 6 January 2018

@copying
This manual and specification for the Dyno test library for guile (version @value{VERSION}, updated @value{UPDATED})

Copyright 2018 Holger Peters

@quotation
@c For more information, see COPYING.docs in the dyno
@c distribution.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
@end quotation
@end copying

@dircategory The Algorithmic Language Scheme
@direntry
* Dyno: (dyno.info). A test-runner for Guile scheme
@end direntry

@titlepage
@title Dyno
@subtitle version @value{VERSION}, updated @value{UPDATED}
@author Holger Peters
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@ifnottex
@node Top
@top Dyno


@insertcopying
@menu
* Introduction::          What's this all about?
* Reference::             API reference.
* Status::                Status of development
* Concept Index::         Dyno is a work in progress.
* Function Index::        Dyno is a work in progress.
@end menu

@end ifnottex

@iftex
@shortcontents
@end iftex

@node Introduction
@chapter Introduction

Dyno is a test-runner for Guile scheme, inspired by Brian Marick's
@url{https://github.com/marick/Midje, Midje}.  The general idea of
Midje, and thus Dyno is, that tests appear similarly to how
expressions and results are represented in a REPL session. The
expression under test is separeted by @code{=>} from the value that it
should evaluate to.  A simple test case asserting that the numbers
``one'' and ``two'' add up to ``two'' (which of course they don't)
looks as follows:

@example
(dy/fact "numbers should add up"
  (+ 1 2) => 2)
@end example

When evaluated, Dyno will report the test failure with contextual
information about what went wrong.

@example
ERROR "numbers should add up: (+ 1 2) => 2" at (...)
    Expected: 2
      Actual: 3
   asserting: (+ 1 2) => 2

...
@end example

@node Reference
@chapter Reference

@defspec dy/fact description expr1 => expected1 [expr2 => expected2 @dots{}]
@defspecx dy/facts description expr1 => expected1 [expr2 => expected2 @dots{}]
expects that @var{exprn} evaluates to @var{expectedn}. @var{dy/facts} is an alias for @var{dy/fact}.
@end defspec

A test module will look something like the following:

@example
(define-module (tests testing)
  #:use-module (dyno testing))

(dy/fact "cons should create a pair"
  (cons 1 2) => (cons 1 2)
  (cdr (list 1)) => '()
  (car (list 1)) => 1)
@end example


@defspec dy/unfinished name1 [name2 @dots{}]
This form implements dummy functions for the given function names @var{name1} @dots{}, that throw an error when they are evaluated.
@end defspec


@defspec dy/assert expression message
@defspecx dy/assert (= lhs rhs) message
The @code{dy/assert} form is an assertion which potentially deconstructs the
asserted expression @code{exp}.
@end defspec

@node Status
@chapter Project status

Dyno is in the early stage of development.

@node Concept Index
@unnumbered Concept Index
@printindex cp

@node Function Index
@unnumbered Function Index
@printindex fn

@bye
