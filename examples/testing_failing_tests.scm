(use-modules (dyno))


(dy/fact "numbers should add up"
  (+ 1 2) => 2)
