(use-modules (dyno))

(dy/unfinished add-two-numbers)

(dy/fact "numbers should add up"
  (add-two-numbers 1 2) => 3)
