(define-module (tests testing)
  #:use-module (dyno))

(dy/fact "cons should create a pair"
  (cons 1 2) => (cons 1 2)
  (cdr (list 1)) => '()
  (car (list 1)) => 1)


(dy/facts "facts is a synonym for fact"
  (cons 1 2) => (cons 1 2)
  (cdr (list 1)) => '()
  (car (list 1)) => 1)



(dy/facts "nested works"
  (dy/fact "constructor forms a pair"
    (cons 1 2) => '(1 . 2))
  (dy/fact "constructor forms a pair"
    (cons 1 2) => '(1 . 2)))
